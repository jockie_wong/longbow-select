﻿## Bootstrap 风格下拉框

Bootstrap 风格页面中Select在不同的浏览器下呈现各有不同，但是相同的一点是非常的丑陋，由于表单录入、数据展示时下拉框经常与文本框一同使用，下拉框在form-control样式下与文本框宽度不一致，使用起来非常的别扭，本人利用文本框改造了一个下拉框样式使用起来非常方便与美观，与文本框一起使用非常完美。

## 在线演示
单页面演示：http://longbowenterprise.gitee.io/longbow-select/  
项目内演示：http://argo.zylweb.cn/ (本项目为开源后台管理框架 [[BootstrapAdmin](https://gitee.com/LongbowEnterprise/BootstrapAdmin)])  

## 快速开始

### 组件依赖 jQuery bootstrap font-awesome

### CSS

```html
<link href="https://cdn.bootcss.com/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
<link href="https://cdn.bootcss.com/font-awesome/5.7.2/css/all.min.css">
<link href="./disk/longbow-select.css">
```
将引入样式表的 &lt;link&gt; 标签复制并粘贴到 &lt;head&gt; 中，并放在所有其他样式表之前。

### JS

```html
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
<script src="./disk/longbow-select.js"></script>
```

将引入脚本的 &lt;script&gt; 标签复制并粘贴到 &lt;body&gt; 最后面。

## 用法

添加网页Html元素

```html
<select id="test" class="d-none">
    <option value="1">北京</option>
    <option value="2">上海</option>
    <option value="3">深圳</option>
</select>
```

## API

### 通过 javascript 初始化控件

```html
<select id="test" class="d-none">
    <option value="1">北京</option>
    <option value="2">上海</option>
    <option value="3">深圳</option>
</select>
<script>
    $('#test').lgbSelect();
</script>   
```

### Options

可以根据自己需要设置占位符，边框颜色等

```html
<select id="test" class="d-none">
    <option value="1">北京</option>
    <option value="2">上海</option>
    <option value="3">深圳</option>
</select>
<script>
    $('#test').lgbSelect({ borderClass: 'primary' });
</script>   
```

## Issue
请前往 [Issue](../../issues) 页面添加问题

## 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request